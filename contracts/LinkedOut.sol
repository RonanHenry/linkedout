pragma solidity >=0.4.21 <0.6.0;

contract LinkedOut {
  enum Roles {
    HUMAN_RESOURCES,
    TRAINING_CENTER,
    WORKER
  }

  struct User {
    string firstName;
    string lastName;
    string email;
    Roles role;
  }

  struct Company {
    string name;
  }

  struct Job {
    string name;
    uint256 start_date;
    uint256 end_date;
    Company company;
  }

  struct JobOffer {
    string name;
    Company company;
  }

  struct Training {
    string name;
    bool certified;
    TrainingCenter trainingCenter;
  }

  struct TrainingCenter {
    string name;
  }

  struct Project {
    string name;
  }

  address public owner;
  mapping (address => User) users;
  mapping (address => Job[]) jobs;
  mapping (address => Training[]) trainings;
  mapping (address => JobOffer[]) jobOffers;
  mapping (address => Project[]) projects;
  address[] public accounts;

  constructor() public {
    owner = msg.sender;
  }

  modifier restricted() {
    require(msg.sender == owner, "Restricted to owner"); _;
  }

  function getAccounts() public view returns(address[] memory) {
    return accounts;
  }

  function setUser(
    address _address,
    string memory _firstName,
    string memory _lastName,
    string memory _email,
    Roles _role) public
  {
    users[_address].firstName = _firstName;
    users[_address].lastName = _lastName;
    users[_address].email = _email;
    users[_address].role = _role;
    accounts.push(_address);
  }

  function getUser(address _address) public view returns (
    string memory,
    string memory,
    string memory,
    Roles
    )
  {
    return (
      users[_address].firstName,
      users[_address].lastName,
      users[_address].email,
      users[_address].role
    );
  }

  function addJobForUser(
    address _address,
    string memory _name,
    uint256 _start_date,
    uint256 _end_date,
    string memory _companyName) public
  {
    uint length = jobs[_address].push(
      Job({
        name: _name,
        start_date: _start_date,
        end_date: 0,
        company: Company({
          name: _companyName
        })
      })
    );

    if (length > 1) {
      jobs[_address][length - 2].end_date = _end_date;
    }
  }

  function getJobForUser(
    address _address,
    uint8 index) public view returns (
    string memory,
    uint256,
    uint256,
    string memory
    )
  {
    uint length = jobs[_address].length;

    if (index < length) {
      Job[] memory jobz = jobs[_address];
      return (
        jobz[index].name,
        jobz[index].start_date,
        jobz[index].end_date,
        jobz[index].company.name
      );
    }
    else {
      return ("", 0, 0, "");
    }
  }
}
