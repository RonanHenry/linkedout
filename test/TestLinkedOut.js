const LinkedOut = artifacts.require("LinkedOut");

contract('Test LinkedOut Blockchain', async (accounts) => {
  let instance;

  beforeEach('Setup before each test', async () => {
    instance = await LinkedOut.deployed();
    await instance.setUser(accounts[0], "Ronan", "Henry", "ronan.henry@imie.fr", 0);
  });

  it("should add a new user in the blockchain", async () => {
    let user = await instance.getUser.call(accounts[0]);
    assert.equal(user[0], "Ronan");
    assert.equal(user[1], "Henry");
    assert.equal(user[2], "ronan.henry@imie.fr");
    assert.equal(user[3], 0);
  });

  it("shoud add a new job to a user", async () => {
    await instance.addJobForUser(accounts[0], "Développeur", 1567172777, 1567172777, "Cap Gemini");
    let job = await instance.getJobForUser.call(accounts[0], 0);
    assert.equal(job[0], "Développeur");
    assert.equal(job[1], 1567172777);
    assert.equal(job[2], 0);
    assert.equal(job[3], "Cap Gemini");
  });
})
